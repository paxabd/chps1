<?php
/**
 * @category CH
 * @package PS1
 * @copyright CyberHull 2018
 * @author Rashid Suleymanov <rashid.suleymanov@cyberhull.com>
 */
namespace CH\PS1\Console;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Framework\Component\ComponentRegistrar;

/**
 * Class Run
 */
class Run extends Command
{
    /**
     *  Run command 
     */
    const RUN_CMD = "%s/ePSX/ePSXe -f -loadiso %s/ePSX/CTR/CTR.iso -nogui";
    const MODULE_NAME = "CH_PS1";

    const COMMAND_NAME = "ch:ps1:run";
    const COMMAND_DESC = "Run PS1 Game";
    /**
     * @param ComponentRegistrar $componentRegistrar
     */
    public function __construct(ComponentRegistrar $componentRegistrar)
    {
        $this->componentRegistrar = $componentRegistrar;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription(self::COMMAND_DESC);
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // find module absolute path
        $path = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, self::MODULE_NAME);
        // generate executable string
        $cmd = sprintf(self::RUN_CMD,$path,$path);
        // run shell
        shell_exec($cmd);
    }
}