<?php
/**
 * @category CH
 * @package PS1
 * @copyright CyberHull 2018
 * @author Rashid Suleymanov <rashid.suleymanov@cyberhull.com>
 */
use \Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
	ComponentRegistrar::MODULE,'CH_PS1',__DIR__
);